from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

def test_user(host):
    assert host.group("www-data").exists
    assert "www" in host.user("www").groups

def test_service(host):
    s = host.service("supervisord")
    assert s.is_enabled
    assert s.is_running
