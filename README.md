Gunicorn
=========
Installs Supervisor: A Process Control System.  

Requirements:  
* python3

Include role
------------
```yaml
- name: supervisor 
  src: https://gitlab.com/ansible_roles_v/supervisor/  
  version: main  
```

Example Playbook
----------------
```yaml
- hosts: hosts
  gather_facts: true
  become: true
  roles:
    - supervisor
```